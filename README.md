# Gitlab MR status in polybar
![](example_image.png)

This module will list all open MRs that you've created in a project. Each MR is listed using the name of its source branch, color coded based on the status of the MR's pipeline. Clicking on a name will open the associated URL in your browser.

## Installation

Install [Python3](https://www.python.org/downloads/) and `pip`

Install [python-gitlab](https://github.com/python-gitlab/python-gitlab):
```pip3 install python-gitlab```

Clone the repo:
`git clone https://gitlab.com/samkhal/polybar-gitlab.git`

Copy `python-gitlab.cfg.template` to `python-gitlab.cfg` and insert your gitlab API token. If you're not on gitlab.com, replace the URL as well.


## Polybar module

Add this code to your polybar config. Replace the appropriate values:
- `<path-to-polybar-gitlab>` with the actual path
- `<project-id>` with your project's id, found on the project's gitlab homepage
- `<user-id>` with your user id, which you can find by going to `https://gitlab.com/api/v4/users?username=<your_username>`

```
[module/gitlab-mr-status]
type = custom/script

exec = <path-to-polybar-gitlab>/polybar-gitlab/mr_status.py --project-id <project-id> --user-id <user-id> --format polybar

; Seconds to sleep between updates
interval = 60
```

## Extra Options

By default, the color coding will be based on the status of the first (most recent) pipeline on that MR. If you supply the `--job-name <name>` argument, it will instead look through the jobs of that pipeline and use the status of the first job matching that name instead.