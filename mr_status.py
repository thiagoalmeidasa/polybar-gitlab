#!/usr/bin/env python3

import gitlab
import argparse
import os
import traceback
import requests

dirname = os.path.dirname(__file__)

color_good = "008000"
color_wait = "ffff00"
color_bad = "FF0000"
color_no_info = "FFFFFF"

status_colors = {
    "created": color_wait,
    "running": color_wait,
    "pending": color_wait,
    "success": color_good,
    "failed": color_bad,
    "canceled": color_bad,
    "skipped": color_bad,
    "": color_no_info
}


def polybar_format(string, color=None, click_url=None):
    """ Format a string for polybar (https://github.com/jaagr/polybar/wiki/Formatting)
    color: 3 or 6 digit hex code
    click_url: url to open when this text is clicked
    """
    formatted_str = string
    if color:
        formatted_str = "%{{F#{color}}}{string}%{{F-}}".format(
            color=color, string=formatted_str)
    if click_url:
        # command = "xdg-open {}".format(click_url)
        # colon must be escaped in polybar commands
        # command = command.replace(":", r"\:")

        # The above doesn't work because of a polybar bug fixed in v3.0.5,
        # so earlier versions can use x-www-browser because it can handle urls without the protocol prefix
        command = "x-www-browser {}".format(
            click_url.replace("http://", "").replace("https://", ""))
        formatted_str = "%{{A1:{command}:}}{string}%{{A}}".format(
            command=command, string=formatted_str)
    return formatted_str


def get_open_mrs(project_id, author_id):
    gl = gitlab.Gitlab.from_config(
        config_files=[os.path.join(dirname, 'python-gitlab.cfg')])

    project = gl.projects.get(project_id, lazy=True)
    return project, project.mergerequests.list(
        state="opened", order_by="updated_at", author_id=author_id)


def get_mr_status(project, mr, job_name=None):
    pipelines = mr.pipelines(lazy=True)
    if not pipelines:
        return ""

    if not job_name:
        return pipelines[0]["status"]
    else:
        pipeline = project.pipelines.get(pipelines[0]["id"], lazy=True)
        jobs_matching_name = [
            job for job in pipeline.jobs.list() if job.name == job_name]
        if jobs_matching_name:
            # If multipe jobs match the name, use the one that started last
            jobs_matching_name.sort(
                key=lambda x: x.started_at if x.started_at else "z")
            return jobs_matching_name[-1].status

        return ""


def format_mr_string(mr, status, fmt="terminal"):
    if fmt == "terminal":
        return "{}:{}".format(mr.source_branch, status)

    elif fmt == "polybar":
        return polybar_format(mr.source_branch, status_colors[status], mr.web_url)


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(
            description="Get info about open MRs")
        parser.add_argument('--project-id', required=True,
                            help="Gitlab project ID (shown on the project homepage)")
        parser.add_argument('--user-id', required=True,
                            help="Your user ID (find it by going to https://gitlab.com/api/v4/users?username=<your_username>")
        parser.add_argument('--format', choices=[
                            "terminal", "polybar"], default="terminal", help="How to format the output")
        parser.add_argument('--job-name',  default="",
                            help="If set, will use the status of a specific job in the first pipeline instead of the first pipeline's status")
        args = parser.parse_args()

        project, my_open_mrs = get_open_mrs(args.project_id, args.user_id)

        mr_strings = []
        for mr in my_open_mrs:
            status = get_mr_status(project, mr, job_name=args.job_name)
            mr_strings.append(format_mr_string(mr, status, fmt=args.format))

        print("|".join(mr_strings))

    except requests.exceptions.ConnectTimeout as e:
        print("Connection to gitlab timed out")

        logfile = os.path.join(dirname, 'out.log')
        with open(logfile, 'w') as f:
            traceback.print_exc(None, f)

    except Exception as e:
        logfile = os.path.join(dirname, 'out.log')
        with open(logfile, 'w') as f:
            traceback.print_exc(None, f)
            print("Exception: Logfile written to {}".format(logfile))
